﻿
using System.Drawing;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using Discord.Data.JsonObjects;
using Discord.Interface;

namespace Discord
{
    public class WebhookManager : IWebhookManager
    {
        public string WebhookUrl {get; set;}

        public WebhookManager()
        {
            WebhookUrl = "empty";
        }

        /// <summary>
        /// Create a webhook class with an assigned WebhookUrl
        /// </summary>
        /// <param name="url"></param>
        public WebhookManager(string url)
        {
            WebhookUrl = url;
        }

        /// <summary>
        /// Posts an image using the assigned WebhookUrl
        /// </summary>
        /// <param name="title"></param>
        /// <param name="color"></param>
        /// <param name="author"></param>
        /// <param name="authorUrl"></param>
        /// <param name="authorIconUrl"></param>
        /// <param name="timestamp"></param>
        /// <param name="ImageUrl"></param>
        /// <returns></returns>
        public Boolean PostImageUrl(string title, string? description, int color, string author,
            string? authorUrl, string? authorIconUrl, DateTime timestamp,
            string imageUrl, Boolean wait = true)
        {
            ExecuteWebhook webhookObject = new ExecuteWebhook
            {
                content = " ",
                embeds = new List<Embed>{
                    new() {
                        title = title,
                        description = description,
                        color = color,
                        author = new Author
                        {
                        name = author,
                        url = authorUrl,
                        icon_url = authorIconUrl
                        },
                        timestamp = timestamp,
                        image = new Image
                        {
                            url = imageUrl
                        }
                    }
                }
            };

            JsonSerializerOptions options = new()
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                MaxDepth = 64,
                WriteIndented = true
            };

            HttpClient client = new HttpClient();
            var jsonString = JsonSerializer.Serialize(webhookObject, options);
            Console.WriteLine($"Posting:\n{jsonString}");

            var response = client.PostAsJsonAsync<ExecuteWebhook>(WebhookUrl+"?wait=true", webhookObject);
            response.Wait();
            var status = response.Status;
            var statusCode = response.Result.StatusCode;

            if (status != TaskStatus.RanToCompletion)
            {
                Console.WriteLine($"Error: {status}");
                return false;
            }

            if (statusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine($"HTTP Response: {statusCode}");
                return false;
            }
            
            return true;
        }
    }
}
