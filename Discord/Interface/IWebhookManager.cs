using System;

namespace Discord.Interface
{
    internal interface IWebhookManager
    {
        public Boolean PostImageUrl(string title, string? description, int color, string author,
            string? authorUrl, string? authorIconUrl, DateTime timestamp,
            string imageUrl, Boolean wait = true);
    }
}
