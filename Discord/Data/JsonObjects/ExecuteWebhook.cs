namespace Discord.Data.JsonObjects
{
    public class ExecuteWebhook
    {
        public string? content { get; set; }
        public string? username { get; set; }
        public string? avatar_url { get; set; }
        public Boolean? tts { get; set; }
        public List<Embed>? embeds { get; set; }
    }
}


