namespace Discord.Data.JsonObjects
{
    public class Author
    {
        public required string name { get; set; }
        public string? url { get; set; }
        public string? icon_url { get; set; }
    }
}


