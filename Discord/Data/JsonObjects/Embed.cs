namespace Discord.Data.JsonObjects;

public class Embed
{
    public string? title { get; set; }
    public string? description { get; set;}
    public int? color { get; set; }
    public Author? author { get; set; }
    public DateTime? timestamp { get; set; }
    public Image? image { get; set; }
    
}
