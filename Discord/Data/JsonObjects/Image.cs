namespace Discord.Data.JsonObjects{
    public class Image
    {
        public required string url { get; set; }
    }
}
