# Twitter Photos to Discord
A simple program that extracts the latest posts from @hyxpk.bsky.social using a specified query that filters for media. Each post's url is then posted into a discord channel via webhook.<br>

## Info json
This file needs to be passed when calling the tool<br>
bluesky_handle - eg. user.bsky.social<br>
feed_limit - number of posts to query for<br>
archive_file_path - where to save post info if a posting to discord is successful. JSON file<br>
info_file_path - where data is saved to reference for later. JSON file<br>

- lastPostTimestamp - Tool only sends posts that are created after this timestamp<br>

discord_webhook - discord webhook url<br>

## Compiling
This uses .NET SDK 8.0
Call dotnet build in this directory to build

## How to use

**WIP**

