﻿using Bluesky.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bluesky.Interface
{

    internal interface IFeedCommands
    {
        /// <summary>
        /// Gets a feed from a specified user
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="filter"></param>
        /// <param name="atHandle"></param>
        public FeedOkResponse? GetAuthorFeed(short limit, HttpApi.AuthorFeedFilters filter, string atHandle);
    }
}
