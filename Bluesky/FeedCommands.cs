﻿using Bluesky.Interface;
using Bluesky.Data;
using System.Net.Http.Json;

namespace Bluesky
{
    public class FeedCommands : IFeedCommands
    {
        public FeedCommands()
        {
        }

        public FeedOkResponse? GetAuthorFeed(short limit, HttpApi.AuthorFeedFilters filter, string atHandle)
        {

            if (!HttpApi.EndpointEnumsToStrings.TryGetValue(HttpApi.ApiCalls.GET_AUTHOR_FEED, out string? endpointUrl))
            {
                Console.WriteLine("Invalid api call");
                return null;
            }

            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, $"{endpointUrl}?actor={atHandle}&limit={limit}&filter={filter}");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "Bearer <TOKEN>");
            var response = client.Send(request);

            Task<FeedOkResponse?> getJson;
            FeedOkResponse? feed;
            switch (response.StatusCode)
            {
                case System.Net.HttpStatusCode.OK:
                    getJson = response.Content.ReadFromJsonAsync<FeedOkResponse>();
                    getJson.Wait();
                    feed = getJson.Result;
                    break;
                case System.Net.HttpStatusCode.BadRequest:
                case System.Net.HttpStatusCode.Unauthorized:
                default:
                    Console.WriteLine("Bad!");
                    return null;
            }

            return feed;
        }
    }
}
