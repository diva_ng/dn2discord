﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bluesky.Data
{
    public class HttpApi
    {

        public static readonly string UserProfileBaseUrl = "https://bsky.app/profile";
        public static readonly string PublicApiBaseUrl = "https://public.api.bsky.app/xrpc";

        public enum EmbedTypes
        {
            BLOB,
            IMAGES,
            IMAGES_VIEW
        }
        public enum RecordTypes
        {
            FEED_POST
        }
        public enum ApiCalls
        {
            GET_AUTHOR_FEED
        }

        public enum AuthorFeedFilters
        {
            POSTS_WITH_REPLIES,
            POSTS_NO_REPLIES,
            POSTS_WITH_MEDIA,
            POSTS_AND_AUTHOR_THREAD

        }

        public static readonly Dictionary<string, EmbedTypes> EmbedStringToEnums = new()
        {
            { "blob", EmbedTypes.BLOB },
            { "app.bsky.embed.images", EmbedTypes.IMAGES },
            { "app.bsky.embed.images#view", EmbedTypes.IMAGES_VIEW }
        };

        public static readonly Dictionary<string, RecordTypes> RecordStringToEnums = new()
        {
            { "app.bsky.feed.post", RecordTypes.FEED_POST }
        };

        public static readonly Dictionary<ApiCalls, string> EndpointEnumsToStrings = new()
        {
            { ApiCalls.GET_AUTHOR_FEED, $"{PublicApiBaseUrl}/app.bsky.feed.getAuthorFeed" }
        };

        public static readonly Dictionary<AuthorFeedFilters, string> FilterEnumsToStrings = new()
        {
            { AuthorFeedFilters.POSTS_WITH_REPLIES, "posts_with_replies" },
            { AuthorFeedFilters.POSTS_NO_REPLIES, "posts_no_replies" },
            { AuthorFeedFilters.POSTS_WITH_MEDIA, "posts_with_media" },
            { AuthorFeedFilters.POSTS_AND_AUTHOR_THREAD, "posts_and_author_threads" },
        };
    }
}
