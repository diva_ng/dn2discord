namespace Bluesky.Data.JsonObjects
{
    public class Blockingbylist
    {
        public string? uri { get; set; }
        public string? cid { get; set; }
        public string? name { get; set; }
        public string? purpose { get; set; }
        public string? avatar { get; set; }
        public int? listItemCount { get; set; }
        public List<Label>? labels { get; set; }
        public MutedBlockedViewer? viewer { get; set; }
        public DateTime? indexedAt { get; set; }
    }

}
