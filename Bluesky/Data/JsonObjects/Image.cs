﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bluesky.Data.JsonObjects
{
    public class Image
    {
        public string? thumb {get; set; }
        public string? fullsize { get; set; }
        public string? alt { get; set; }
        public AspectRatio? aspectRatio { get; set; }
        public ImageBlob? image { get; set; }
    }
}
