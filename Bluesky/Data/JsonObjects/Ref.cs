﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Bluesky.Data.JsonObjects
{
    public class Ref
    {
        [JsonPropertyName("$link")]
        public required string link { get; set; }
    }
}
