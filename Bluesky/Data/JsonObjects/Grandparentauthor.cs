namespace Bluesky.Data.JsonObjects
{
    public class Grandparentauthor
    {
        public string? did { get; set; }
        public string? handle { get; set; }
        public string? displayName { get; set; }
        public string? avatar { get; set; }
        public Associated? associated { get; set; }
        public UserViewer? viewer { get; set; }
        public List<Label>? labels { get; set; }
        public DateTime? createdAt { get; set; }
    }
}
