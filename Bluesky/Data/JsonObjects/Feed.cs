namespace Bluesky.Data.JsonObjects
{
    public class Feed
    {
        public required Post post { get; set; }
        public Reply? reply { get; set; }
        public Reason? reason { get; set; }
        public string? feedContext { get; set; }
    }
}
