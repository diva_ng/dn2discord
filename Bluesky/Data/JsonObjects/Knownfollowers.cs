namespace Bluesky.Data.JsonObjects
{
    public class Knownfollowers
    {
        public int? count { get; set; }
        public List<object>? followers { get; set; }
    }
}
