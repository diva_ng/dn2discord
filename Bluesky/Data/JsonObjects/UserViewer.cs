namespace Bluesky.Data.JsonObjects
{
    public class UserViewer
    {
        public bool? muted { get; set; }
        public MutedByList? mutedByList { get; set; }
        public bool? blockedBy { get; set; }
        public string? blocking { get; set; }
        public Blockingbylist? blockingByList { get; set; }
        public string? following { get; set; }
        public string? followedBy { get; set; }
        public Knownfollowers? knownFollowers { get; set; }
    }
}
