namespace Bluesky.Data.JsonObjects
{
    public class Threadgate
    {
        public string? uri { get; set; }
        public string? cid { get; set; }
        public Record? record { get; set; }
        public List<BlueSkyList>? lists { get; set; }
    }
}
