namespace Bluesky.Data.JsonObjects
{
    public class Reply
    {
        public Root? root { get; set; }
        public Parent? parent { get; set; }
        public Grandparentauthor? grandparentAuthor { get; set; }
    }
}
