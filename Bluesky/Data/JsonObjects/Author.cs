namespace Bluesky.Data.JsonObjects
{
    public class Author
    {
        public required string did { get; set; }
        public required string handle { get; set; }
        public string? displayName { get; set; }
        public string? avatar { get; set; }
        public Associated? associated { get; set; }
        public Viewer? viewer { get; set; }
        public List<Label>? labels { get; set; }
        public DateTime? createdAt { get; set; }
    }
}
