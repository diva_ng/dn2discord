namespace Bluesky.Data.JsonObjects
{
    public class MutedBlockedViewer
    {
        public bool? muted { get; set; }
        public string? blocked { get; set; }
    }
}
