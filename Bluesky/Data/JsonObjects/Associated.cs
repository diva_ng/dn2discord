namespace Bluesky.Data.JsonObjects
{
    public class Associated
    {
        public int? lists { get; set; }
        public int? feedgens { get; set; }
        public int? starterPacks { get; set; }
        public bool? labeler { get; set; }
        public Chat? chat { get; set; }
    }
}
