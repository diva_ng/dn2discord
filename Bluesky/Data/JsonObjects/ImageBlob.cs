using System.Text.Json.Serialization;

namespace Bluesky.Data.JsonObjects
{
    public class ImageBlob
    {
        [JsonPropertyName("$type")]
        public required string type { get; set; }
        [JsonPropertyName("ref")]
        public Ref? rref { get; set; }
        public string? mimeType { get; set; }
        public int? size { get; set; }
    }
}


