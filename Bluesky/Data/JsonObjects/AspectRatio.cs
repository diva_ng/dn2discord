﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bluesky.Data.JsonObjects
{
    public class AspectRatio
    {
        public int height { get; set; }
        public int width { get; set; }
    }
}
