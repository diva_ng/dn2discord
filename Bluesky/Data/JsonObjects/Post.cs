namespace Bluesky.Data.JsonObjects
{
    public class Post
    {
        public required string uri { get; set; }
        public required string cid { get; set; }
        public required Author author { get; set; }
        public required Record record { get; set; }
        public Embed? embed { get; set; }
        public int? replyCount { get; set; }
        public int? repostCount { get; set; }
        public int? likeCount { get; set; }
        public int? quoteCount { get; set; }
        public required DateTime indexedAt { get; set; }
        public ResponseViewer? viewer { get; set; }
        public List<Label>? labels { get; set; }
        public Threadgate? threadgate { get; set; }
    }
}
