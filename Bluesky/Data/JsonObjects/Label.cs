namespace Bluesky.Data.JsonObjects
{
    public class Label
    {
        public int? ver { get; set; }
        public string? src { get; set; }
        public string? uri { get; set; }
        public string? cid { get; set; }
        public string? val { get; set; }
        public bool? neg { get; set; }
        public DateTime? cts { get; set; }
        public DateTime? exp { get; set; }
        public string? sig { get; set; }
    }
}
