using System.Text.Json.Serialization;

namespace Bluesky.Data.JsonObjects
{
    public class Embed
    {

        [JsonPropertyName("$type")]
        public required string type { get; set; }
        public List<Image>? images { get; set; }
    }
}
