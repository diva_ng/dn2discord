namespace Bluesky.Data.JsonObjects
{
    public class ResponseViewer
    {
        public string? repost { get; set; }
        public string? like { get; set; }
        public bool? threadMuted { get; set; }
        public bool? replyDisabled { get; set; }
        public bool? embeddingDisabled { get; set; }
        public bool? pinned { get; set; }
    }
}
