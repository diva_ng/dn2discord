using System.Text.Json.Serialization;

namespace Bluesky.Data.JsonObjects
{
    public class Record
    {
        [JsonPropertyName("$type")]
        public required string type { get; set; }
        public DateTime? createdAt { get; set; }
        public Embed? embed { get; set; }
        public List<string>? langs { get; set; }
        public string? text { get; set; }
    }
}
