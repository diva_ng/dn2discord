using Bluesky.Data.JsonObjects;
using System.Text;

namespace Bluesky.Data
{
    public class FeedOkResponse
    {
        public string? cursor { get; set; }
        public List<Feed>? feed { get; set; }

        private string unrollEmbed(Embed embed)
        {
            StringBuilder sb = new StringBuilder();
            
            if (!HttpApi.EmbedStringToEnums.TryGetValue(embed.type, out var type))
            {
                sb.Append($"Unknown Type: {embed.type}\n");
            }
            else
            {
                switch (type)
                {
                    case HttpApi.EmbedTypes.IMAGES_VIEW:
                        var images = embed.images;
                        if (images != null)
                        {
                            foreach (var image in images)
                            {
                                sb.Append($"Thumbnail Url: {image.thumb}\n");
                                sb.Append($"Image Url: {image.fullsize}\n");
                            }
                        }
                        break;
                    case HttpApi.EmbedTypes.BLOB:
                    default:
                        sb.Append($"Can't handle {type}\n");
                        break;
                }
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (feed == null)
            {
                return "Empty";
            }
            foreach (Feed item in feed)
            {
                sb.Append($"Item: \n");
                if (!HttpApi.RecordStringToEnums.TryGetValue(item.post.record.type, out HttpApi.RecordTypes type))
                {
                    sb.Append($"\tUnknown Record Type: {item.post.record.type}");
                }

                var author = item.post.author.displayName;
                var post = item.post.record;
                var embeds = item.post.embed;
                switch (type)
                {
                    case HttpApi.RecordTypes.FEED_POST:                        
                        sb.Append($"\tAuthor: {author}\n");
                        sb.Append($"\tcreatedAt: {post.createdAt}\n");

                        if (post.text != null)
                        {
                            var formattedText = post.text.Replace("\n", " ");
                            sb.Append($"\tText: {formattedText}\n");
                        }

                        if (embeds != null)
                        {
                            sb.Append($"\tEmbeds:\n");
                            var unrolledEmbeds = unrollEmbed(embeds);
                            unrolledEmbeds = "\t\t" + unrolledEmbeds.Replace("\n", "\n\t\t");
                            sb.Append($"{unrolledEmbeds}\n");
                            

                        }
                        break;
                    default:
                        sb.Append($"\tCan't handle {type}\n");
                        break;
                }
            }
            return sb.ToString();
        }
    }
}
