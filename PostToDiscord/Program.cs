﻿using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Bluesky;
using Bluesky.Data.JsonObjects;
using Discord;

internal class PostToDiscord
{

    private static void Usage()
    {
        Console.WriteLine("Arguments:");
        Console.WriteLine("\t-i/--infoFile: Json file containing the bluesky handle and webhook.");
    }

    private class PostConfig()
    {
        public required string bluesky_handle { get; set; }
        public required string info_file_path { get; set; }
        public string? archive_file_path { get; set; }
        public int? feed_limit { get; set; }
        public required string discord_webhook { get; set; }
    }

    private class InfoSave()
    {
        public required DateTime lastPostTimestamp { get; set; }
    }

    
    private class ArchiveObject()
    {
        public required List<ArchiveEntryObject> data { get; set; }
    }
    
    /// <summary>
    /// Old entry format ported from twitter.
    /// </summary>
    private class ArchiveEntryObject()
    {
        public required int index { get; set; }
        public required string text { get; set; }
        public string? media_key { get; set; }
        public required string url { get; set; }
        public string? expanded_url { get; set; }
        public required string created_at { get; set; }
    }

    private static PostConfig? GetInfoMetaFromFile(string filepath)
    {
        PostConfig? infoMeta = null;
        try
        {
            using StreamReader reader = new StreamReader(filepath);
            string json = reader.ReadToEnd();
            infoMeta = JsonSerializer.Deserialize<PostConfig>(json);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        return infoMeta;
    }

    private static InfoSave? GetInfoSaveFromFile(string filepath)
    {
        InfoSave? infoSave = null;
        try
        {
            using StreamReader reader = new StreamReader(filepath);
            string json = reader.ReadToEnd();
            infoSave = JsonSerializer.Deserialize<InfoSave>(json);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        return infoSave;
    }

    private static ArchiveObject? GetArchiveFromFile(string filepath)
    {
        ArchiveObject? archive = null;
        try
        {
            using StreamReader reader = new StreamReader(filepath);
            string json = reader.ReadToEnd();
            archive = JsonSerializer.Deserialize<ArchiveObject>(json);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        return archive;
    }

    public static void Main(string[] args)
    {
        Boolean postPosted = false;
        if (args.Length == 0)
        {
            Usage();
            System.Environment.Exit(255);
        }

        string? infoFilePath = null;
        for (int i = 0; i < args.Length; i++)
        {
            string option = args[i];
            switch (option)
            {
                case "-i": case "--infoFile":
                    if (++i >= args.Length)
                    {
                        Console.WriteLine($"{args[i - 1]} needs an argument.");
                    }
                    infoFilePath = args[i];
                    break;
                default:
                    Console.WriteLine($"Invalid option {option}");
                    Usage();
                    System.Environment.Exit(255);
                    break;
            }
        }

        if (infoFilePath == null)
        {
            Console.WriteLine($"Need to pass an info file");
            System.Environment.Exit(255);
        }

        PostConfig? config = GetInfoMetaFromFile(infoFilePath);
        if (config == null)
        {
            Console.WriteLine($"Bad file {infoFilePath}");
            System.Environment.Exit(255);
        }

        InfoSave? infoSave = GetInfoSaveFromFile(config.info_file_path);
        if (File.Exists(config.info_file_path))
        {
            if (infoSave == null)
            {
                Console.WriteLine($"Bad file {config.info_file_path}");
                System.Environment.Exit(255);
            }
        }
        else
        {
            InfoSave newInfoSave = new InfoSave
            {
                lastPostTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            };
            using FileStream newFileStream = File.Create(config.info_file_path);
            var fileWriter = JsonSerializer.SerializeAsync(newFileStream, newInfoSave);
            fileWriter.Wait();
            infoSave = newInfoSave;
        }

        ArchiveObject? archive = (config.archive_file_path == null) ? null : GetArchiveFromFile(config.archive_file_path);
        if (config.archive_file_path != null)
        {
            if (File.Exists(config.archive_file_path))
            {
                if (archive == null)
                {
                    Console.WriteLine($"Bad file {config.archive_file_path}");
                    System.Environment.Exit(255);
                }
            }
            else
            {
                ArchiveObject newArchiveObject = new ArchiveObject
                {
                    data = new List<ArchiveEntryObject>(0)
                };
                using FileStream newFileStream = File.Create(config.archive_file_path);
                var fileWriter = JsonSerializer.SerializeAsync(newFileStream, newArchiveObject);
                fileWriter.Wait();
                archive = newArchiveObject;
            }
        }
    
        var feedLimit = (config.feed_limit != null) ? config.feed_limit : 1;
        FeedCommands feed = new FeedCommands();
        var feedData = feed.GetAuthorFeed((short)feedLimit, Bluesky.Data.HttpApi.AuthorFeedFilters.POSTS_WITH_MEDIA, config.bluesky_handle);
        if (feedData == null)
        {
            System.Environment.Exit(255);
        }

        WebhookManager discordPoster = new WebhookManager(config.discord_webhook);

        if (feedData.feed == null)
        {
            System.Environment.Exit(255);
        }

        string authorUrl = $"{Bluesky.Data.HttpApi.UserProfileBaseUrl}/{config.bluesky_handle}";
        feedData.feed.Reverse();
        foreach (Feed item in feedData.feed)
        {
            if (item.post.embed != null)
            {
                var post = item.post;
                var title = (post.record.text == null) ? "" : post.record.text;
                var timestamp = (post.record.createdAt == null) ? DateTime.MaxValue : (DateTime)post.record.createdAt;
                var authorName = (post.author.displayName == null) ? "" : post.author.displayName;
                var authorIconUrl = (post.author.avatar == null) ? "" : post.author.avatar;
                var postUri = (post.uri == null) ? "" : post.uri;

                /*
                 *  A mess of nonsense to build the post url from the post uri
                 *  There's most likely a better way to do this
                 */
                Regex regPostStub = new Regex("app\\.bsky\\.feed\\.post/[0-9a-zA-Z]+$");
                var postStubMatch = regPostStub.Match(postUri);
                var postStubResult = postStubMatch.Result;
                string? postStubString = (postStubResult.Target == null || !postStubMatch.Success) ? "" : postStubResult.Target.ToString();
                string[] postStubs = (postStubString == null) ?  [""] : postStubString.Split('/');
                string postUrl = (postStubs.Length == 2) ? $"{Bluesky.Data.HttpApi.UserProfileBaseUrl}/{config.bluesky_handle}/post/{postStubs[1]}" : "";


                // Check if title starts with a number
                Regex regTitle = new Regex("^[0-9]+\\.");

                // Just get the first image
                var image = (post.embed.images == null) ? null : post.embed.images[0];
                var imageUrl = (image == null) ? "" : (image.thumb == null) ? "" : image.thumb;

                var lowerBound = new TimeSpan(15,55,0);
                var upperBound = new TimeSpan(22,00,0);
                var timestampTime = timestamp.TimeOfDay;

                if (
                    regTitle.IsMatch(title) &&
                    timestamp > infoSave.lastPostTimestamp &&
                    timestampTime > lowerBound &&
                    timestampTime < upperBound
                )
                {
                    // Something is strange happening where Discord isn't embedding the image
                    // Assuming that Bluesky needs time to process a new post so wait for a bit
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                    var posted = discordPoster.PostImageUrl
                    (
                        title: title,
                        description: $" [Bluesky]({postUrl})",
                        color: 0x1085ff,
                        author: authorName,
                        authorUrl: authorUrl,
                        authorIconUrl: authorIconUrl,
                        timestamp: (DateTime)timestamp,
                        imageUrl: imageUrl
                    );

                    if (posted)
                    {
                        postPosted = true;
                        using FileStream saveStream = File.Create(config.info_file_path);
                        infoSave.lastPostTimestamp = timestamp;
                        var fileWriter = JsonSerializer.SerializeAsync(saveStream, infoSave);
                        fileWriter.Wait();

                        // This is stupid. Figure out how to do this better
                        if (archive != null && config.archive_file_path != null)
                        {
                            /*
                            *  A mess of nonsense to isolate the image number
                            */
                            var regIndexResult = regTitle.Match(title).Result;
                            string? indexString = (regIndexResult.Target == null) ? "" : regIndexResult.Target.ToString();
                            string[] numStub = (indexString == null) ? [""] : indexString.Split('.');
                            int index = int.Parse(numStub[0]);

                            ArchiveEntryObject newEntry = new ArchiveEntryObject
                            {
                                index = index,
                                text = title,
                                url = imageUrl,
                                expanded_url = postUrl,
                                created_at = timestamp.ToString("yyyy-MM-ddTHH:mm:ssZ")
                            };


                            TimeSpan epochTimeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1);
                            File.Move(config.archive_file_path, $"{config.archive_file_path}.{(long)epochTimeSpan.TotalMilliseconds}.bak");
                            using FileStream archiveSaveStream = File.Create(config.archive_file_path);
                            archive.data.Add(newEntry);
                            JsonSerializerOptions options = new()
                            {
                                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                                MaxDepth = 64,
                                WriteIndented = true
                            };
                            var archiveFileWrite = JsonSerializer.SerializeAsync(archiveSaveStream, archive, options);
                            archiveFileWrite.Wait();
                        }
                    }
                }
                else
                {
                    Console.Write($"{item.post.cid[0]}{item.post.cid[1]} ");
                }
            }
        }

        if (postPosted)
        {
            System.Environment.Exit(0);
        }
        else
        {
            System.Environment.Exit(255);
        }
    }
}